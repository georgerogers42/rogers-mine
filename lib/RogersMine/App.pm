package RogersMine::App;
use 5.20.0;
use Moo;
use strictures;
use Gtk3;
use RogersMine::MineField;
use namespace::autoclean;

has complete => (is => 'rw', default => sub { 0; });
has rows => (is => 'ro');
has cols => (is => 'ro');
has lives => (is => 'ro');
has risk => (is => 'ro');

has minefield => (is => 'lazy');

sub _build_minefield {
  my $self = shift;
  RogersMine::MineField->new(rows => $self->rows, cols => $self->cols, risk => $self->risk, lives => $self->lives);
}

has info => (is => 'lazy');
has window => (is => 'lazy');

sub _build_info {
  my $self = shift;
  Gtk3::Label->new($self->lives);
}

sub _build_window {
  my $self = shift;
  my $window = Gtk3::Window->new('toplevel');
  $window->set_title('Minefield');
  my $vbox = Gtk3::VBox->new;
  $vbox->add($self->info);
  for my $i (0..$self->rows-1) {
    my $hbox = Gtk3::HBox->new;
    for my $j (0..$self->cols-1) {
      my $btn = Gtk3::Button->new(' ');
      $btn->signal_connect(clicked => sub { $self->click_btn($i, $j, @_) });
      $hbox->add($btn);
    }
    $vbox->add($hbox);
  }
  $window->add($vbox);
  $window;
}

sub show {
  my $self = shift;
  $self->window->show_all;
}

sub click_btn {
  my ($self, $i, $j, $btn, $evt) = @_;
  return if $self->complete;
  my $safe = $self->minefield->click($i, $j);
  if($safe) {
    $btn->set_label($safe);
  } else {
    $self->info->set_text($self->minefield->lives);
    $btn->set_label('*');
  }
  if($self->minefield->complete) {
    $self->complete(1);
    if($self->minefield->lives > 0) {
      $self->info->set_text("You won with @{[$self->minefield->lives]} remaining");
    } else {
      $self->info->set_text("You Lost");
    }
  }
}

1;
